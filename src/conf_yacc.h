/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_CONF_SRC_CONF_YACC_H_INCLUDED
# define YY_CONF_SRC_CONF_YACC_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int confdebug;
#endif
/* "%code requires" blocks.  */
#line 1 "src/conf_yacc.y"

#include "conf_ast.h"

#line 53 "src/conf_yacc.h"

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    TDEFINE = 258,                 /* "@@define"  */
    TUNDEFINE = 259,               /* "@@undef"  */
    TIFDEF = 260,                  /* "@@ifdef"  */
    TIFNDEF = 261,                 /* "@@ifndef"  */
    TIFNHOST = 262,                /* "@@ifnhost"  */
    TIFHOST = 263,                 /* "@@ifhost"  */
    TIF = 264,                     /* "@@if"  */
    TBOOLNOT = 265,                /* "not"  */
    TBOOLFUNC = 266,               /* "boolean operator or function"  */
    TELSE = 267,                   /* "@@else"  */
    TENDIF = 268,                  /* "@@endif"  */
    TINCLUDE = 269,                /* "@@include"  */
    TXINCLUDE = 270,               /* "@@x_include"  */
    TSETENV = 271,                 /* "@@x_include_setenv"  */
    TGROUP = 272,                  /* "group name"  */
    TSTRING = 273,                 /* "string"  */
    TEXPR = 274,                   /* "group"  */
    TVARIABLE = 275,               /* "variable name"  */
    TSPACE = 276,                  /* "whitespace"  */
    TNEWLINE = 277,                /* "new line"  */
    TSELRXRULE = 278,              /* "regular rule"  */
    TEQURXRULE = 279,              /* "equals rule"  */
    TNEGRXRULE = 280,              /* "negative rule"  */
    CONFIGOPTION = 281             /* "configuration option"  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define TDEFINE 258
#define TUNDEFINE 259
#define TIFDEF 260
#define TIFNDEF 261
#define TIFNHOST 262
#define TIFHOST 263
#define TIF 264
#define TBOOLNOT 265
#define TBOOLFUNC 266
#define TELSE 267
#define TENDIF 268
#define TINCLUDE 269
#define TXINCLUDE 270
#define TSETENV 271
#define TGROUP 272
#define TSTRING 273
#define TEXPR 274
#define TVARIABLE 275
#define TSPACE 276
#define TNEWLINE 277
#define TSELRXRULE 278
#define TEQURXRULE 279
#define TNEGRXRULE 280
#define CONFIGOPTION 281

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 42 "src/conf_yacc.y"

  char* s;

  config_option option;

  bool_operator operator;

  ast* ast;

  if_condition* if_cond;
  bool_expression* bool_expr;
  attribute_expression* attr_expr;
  restriction_expression* rs_expr;
  string_expression* string_expr;

#line 141 "src/conf_yacc.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE conflval;


int confparse (ast** config_ast);


#endif /* !YY_CONF_SRC_CONF_YACC_H_INCLUDED  */
