@@if not defined ACNG_DEBIAN_DISTS
@@define ACNG_DEBIAN_DISTS (buster|bullseye|bookworm|trixie|sid)
@@endif
@@if not defined ACNG_SECURITY_DISTS
@@define ACNG_SECURITY_DISTS (bookworm|bullseye)
@@endif
@@if not defined ACNG_UPDATE_DISTS
@@define ACNG_UPDATE_DISTS (stretch|buster|bullseye|bookworm)
@@endif

@@if not defined ACNGCACHE
@@define ACNGCACHE var/cache/apt-cacher-ng
@@endif
@@if not defined ACNGDEB
@@define ACNGDEB (debrep|secdeb|debian(security)?)
@@endif
@@if not defined ACNGDISTS
@@define ACNGDISTS @@{ACNGDEB}/dists/(@@{ACNG_SECURITY_DISTS}(-security)?|@@{ACNG_UPDATE_DISTS}([-/]updates)?|sid|oldstable(-updates)?|stable(-security)?|testing|unstable|experimental)
@@endif
@@if not defined ACNGMNC
@@define ACNGMNC (main|contrib|non-free(-firmware)?)
@@endif
@@if not defined ACNGARCHS
@@define ACNGARCHS (arm64|armhf|amd64|i386|all)
@@endif
@@if not defined ACNGDATENR
@@define ACNGDATENR 20[12][[:digit:]]-[[:digit:]]{2}-[[:digit:]]{2}-[[:digit:]]{4}\\.[[:digit:]]{2}
@@endif
@@if not defined ACNGDTNR
@@define ACNGDTNR (@@{ACNGDATENR}|T-@@{ACNGDATENR}-F-@@{ACNGDATENR})
@@endif

/@@{ACNGCACHE}$ d VarDir-n
!/@@{ACNGCACHE}/_actmp$ d
!/@@{ACNGCACHE}/_actmp/(combined\\.diff|patch\\.base)$ f
/@@{ACNGCACHE}/_(expending_(damaged|dat))$ f VarFile
!/@@{ACNGCACHE}/_(exfail_cnt)$ f
/@@{ACNGCACHE}/_xstore/qstats/[a-z]$ d VarDir
!/@@{ACNGCACHE}/_xstore/qstats/([a-z]/)?[0-9]{10}\\.[0-9]{4,6}$ l
/@@{ACNGCACHE}/_xstore/rsnap/@@{ACNGDISTS}(/@@{ACNGMNC}/binary-@@{ACNGARCHS})?$ d VarDir
!/@@{ACNGCACHE}/_xstore/rsnap/@@{ACNGDISTS}(/@@{ACNGMNC}/binary-@@{ACNGARCHS})?/[[:digit:]]{21,25}$ f 
/@@{ACNGCACHE}/@@{ACNGDEB}$ d VarDir-n
!/@@{ACNGCACHE}/@@{ACNGDISTS}$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}(/@@{ACNGMNC}/binary-@@{ACNGARCHS})?/(In)?Release(~|(\\.gpg)?(\\.hea(d|%))?)?$ f
# the doubled ACNGCACHE is #1050850
!/@@{ACNGCACHE}/(@@{ACNGCACHE}/)?@@{ACNGDEB}/dists$ d
!/@@{ACNGCACHE}/(@@{ACNGCACHE}/)?@@{ACNGDISTS}$ d
!/@@{ACNGCACHE}/(@@{ACNGCACHE}/)?@@{ACNGDISTS}/(In)?Release(\\.[[:digit:]]+)?(\\.head)?$ f
#/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/binary-@@{ACNGARCHS}$ d VarDir
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}(/dep11)?(/by-hash)?$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}(/dep11)?/by-hash/SHA256$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}(/dep11)?/by-hash/SHA256/[[:xdigit:]]{64}(\\.head)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/source/Sources(\\.(gz|xz))?(\\.head|~)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/(source|(Contents|binary)-@@{ACNGARCHS}\\.diff)$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/i18n$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/(Contents-@@{ACNGARCHS}\\.[gx]z)(\\.head|~)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/(debian-installer/)?binary-@@{ACNGARCHS}/(Packages(\\.(gz|bz2|xz))?)(\\.hea(d|%)|~)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/dep11/(Components-@@{ACNGARCHS}\\.yml\\.xz|icons-(128x128|48x48|64x64)\\.tar\\.gz)(\\.head)?$ f
/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/i18n/Translation-(de|en)\\.diff$ d VarDir
/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/(Contents-@@{ACNGARCHS}|Sources\\.diff)$ d VarDir
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/i18n/Translation-(de(_DE)?|en)\\.(bz2|xz)(~|\\.hea(d|%))?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/i18n/Translation-(de|en)(\\.diff)?/Index(\\.head|~)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/Contents-@@{ACNGARCHS}\\.diff/Index(~|\\.head)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/binary-@@{ACNGARCHS}/Packages(\\.diff)?/Index(\\.head|~)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/source/Sources\\.diff/Index(\\.head|~)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/Contents-@@{ACNGARCHS}\\.diff(/by-hash)?$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/Contents-@@{ACNGARCHS}\\.diff/by-hash/SHA256$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/Contents-@@{ACNGARCHS}\\.diff/@@{ACNGDTNR}\\.gz(\\.head)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/Contents-@@{ACNGARCHS}\\.diff/by-hash/SHA256/[[:xdigit:]]{64}(\\.head)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/(debian-installer/)?binary-@@{ACNGARCHS}$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/(debian-installer/)?binary-@@{ACNGARCHS}(/Packages\\.diff)?(/by-hash)?$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/binary-@@{ACNGARCHS}(/Packages\\.diff)?/by-hash/SHA256$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/binary-@@{ACNGARCHS}/Packages\\.diff/@@{ACNGDTNR}\\.gz(\\.head)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/binary-@@{ACNGARCHS}/(Packages\\.diff/)?by-hash/SHA256/[[:xdigit:]]{64}(\\.head)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/i18n(/Translation-(en)\\.diff)?(/by-hash)?$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/i18n(/Translation-(en)\\.diff)?/by-hash/SHA256$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/i18n/Translation-(en)\\.diff/@@{ACNGDTNR}\\.gz(\\.head)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/i18n(/Translation-(en)\\.diff)?/by-hash/SHA256/[[:xdigit:]]{64}(\\.head)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/source(/Sources\\.diff)?(/by-hash)?$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/source(/Sources\\.diff)?/by-hash/SHA256$ d
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/source/Sources\\.diff/@@{ACNGDTNR}\\.gz(\\.head)?$ f
!/@@{ACNGCACHE}/@@{ACNGDISTS}/@@{ACNGMNC}/source(/Sources\\.diff)?/by-hash/SHA256/[[:xdigit:]]{64}(\\.head)?$ f
/@@{ACNGCACHE}/@@{ACNGDEB}/pool$ d VarDir
!/@@{ACNGCACHE}/@@{ACNGDEB}/pool(/updates)?/@@{ACNGMNC}(/(lib)?[a-z0-9](/[-a-z0-9+\\.]+)?)?$ d
!/@@{ACNGCACHE}/@@{ACNGDEB}/pool(/updates)?/@@{ACNGMNC}/(lib)?[[:alnum:]]/[-+[:alnum:]\\.]+/[-+[:alnum:]\\.]+_[-+~[:alnum:]\\.]+_@@{ACNGARCHS}\\.u?deb(~|(\\.hea(d|%)))?$ f
!/@@{ACNGCACHE}/@@{ACNGDEB}/pool(/updates)?/@@{ACNGMNC}/(lib)?[a-z0-9]/[-+[:alnum:]\\.]+/[-+[:alnum:]\\.]+_[-+~[:alnum:]\\.]+\\.(dsc|(diff|((debian|orig)\\.)?tar)\\.(gz|bz2|xz))(\\.asc)?(\\.head)?$ f

/@@{RUN}/apt-cacher-ng$ d RecreatedDir
!/@@{RUN}/apt-cacher-ng/socket$ s

@@define ACNGLOGDIR var/log/apt-cacher-ng
@@define ACNGLOGRE apt-cacher\\.(log|err)

/@@{ACNGLOGDIR}$ d VarDir
!/@@{ACNGLOGDIR}/maint_[[:digit:]]+\\.log\\.html(\\.xz)?$ f
/@@{ACNGLOGDIR}/@@{ACNGLOGRE}$ f ActLog
/@@{ACNGLOGDIR}/@@{ACNGLOGRE}\\.1$ f RotLog
/@@{ACNGLOGDIR}/@@{ACNGLOGRE}\\.2\\.gz$ f CompSerLog
/@@{ACNGLOGDIR}/@@{ACNGLOGRE}\\.3\\.gz$ f MidlSerLog
/@@{ACNGLOGDIR}/@@{ACNGLOGRE}\\.4\\.gz$ f LastSerLog
@@undef ACNG_DEBIAN_DISTS
@@undef ACNG_SECURITY_DISTS
@@undef ACNG_UPDATE_DISTS
@@undef ACNGCACHE
@@undef ACNGDEB
@@undef ACNGDISTS
@@undef ACNGMNC
@@undef ACNGARCHS
@@undef ACNGDATENR
@@undef ACNGDTNR
@@undef ACNGLOGDIR
@@undef ACNGLOGRE
